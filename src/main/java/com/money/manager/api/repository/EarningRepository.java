package com.money.manager.api.repository;

import com.money.manager.api.model.Earning;
import com.money.manager.api.projection.EarningPerMonth;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EarningRepository extends PagingAndSortingRepository<Earning, Long> {

    Page<Earning> findAll(Specification<Earning> specification, Pageable pageable);

    Page<Earning> findAllByUserId(long userId, Pageable pageable);

    @Query(value = "SELECT ((SELECT coalesce(SUM(income), 0) FROM earning e WHERE e.user_id=?1)-\n" +
            "       (SELECT coalesce(SUM(cost), 0) FROM expense exp WHERE exp.user_id=?1)) as profit"
            , nativeQuery = true)
    Integer findUserProfit(long userId);

    @Query(value = "SELECT to_char(ea.date,'MM') AS month,\n" +
            "       EXTRACT(year FROM ea.date) AS year,\n" +
            "       SUM(ea.income) AS income\n" +
            "FROM earning ea\n " +
            "WHERE ea.user_id=?1 AND EXTRACT(year FROM ea.date)=?2 " +
            "GROUP BY 1,2\n" +
            "ORDER BY 1 ASC;"
            , nativeQuery = true)
    List<EarningPerMonth> findEarningPerMonth(long userId, int year);
}
