package com.money.manager.api.repository;

import com.money.manager.api.model.Expense;
import com.money.manager.api.projection.ExpensePerMonth;
import com.money.manager.api.projection.ExpenseStatistic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExpenseRepository extends PagingAndSortingRepository<Expense, Long> {

    Page<Expense> findAll(Specification<Expense> specification, Pageable pageable);

    Page<Expense> findAllByUserId(long userId, Pageable pageable);

    @Query(value = "SELECT exp.category,SUM(exp.cost) as cost FROM expense exp " +
            "WHERE exp.user_id=:userId " +
            "AND (:dateStart like 'empty' OR exp.date>=TO_DATE(:dateStart, 'yyyy-MM-dd')) " +
            "AND (:dateEnd like 'empty' OR exp.date<=TO_DATE(:dateEnd, 'yyyy-MM-dd')) " +
            "GROUP BY exp.category ORDER BY cost DESC"
            , nativeQuery = true)
    List<ExpenseStatistic> findCostSumPerCategoryByDate(@Param("userId") long userId, @Param("dateStart") String dateStart, @Param("dateEnd") String dateEnd);

    @Query(value = "SELECT EXTRACT(year from exp.date) as year\n" +
            "FROM expense exp WHERE exp.user_id=?1\n" +
            "\n" +
            "UNION DISTINCT\n" +
            "SELECT EXTRACT(year from ea.date) as year\n" +
            "FROM earning ea WHERE ea.user_id=?1\n" +
            "\n" +
            "GROUP BY year\n" +
            "ORDER BY year DESC", nativeQuery = true)
    List<Integer> findYears(long userId);

    @Query(value = "SELECT TO_CHAR(exp.date,'MM') as month,\n" +
            "       EXTRACT(year from exp.date) as year,\n" +
            "       SUM(exp.cost) as cost\n" +
            "FROM expense exp\n" +
            "WHERE exp.user_id=?1 AND EXTRACT(year from exp.date)=?2 " +
            "GROUP BY 1,2\n" +
            "ORDER BY 1 ASC;", nativeQuery = true)
    List<ExpensePerMonth> findExpensePerMonth(long userId, int year);

}
