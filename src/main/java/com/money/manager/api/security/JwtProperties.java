package com.money.manager.api.security;

public class JwtProperties {
    public static final String SECRET = "money-manager-2020";
    public static final int EXPIRATION_TIME = 604_800_000; // 7 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}