package com.money.manager.api.controller;

import com.money.manager.api.model.User;
import com.money.manager.api.service.UserService;
import com.money.manager.api.validation.ValidationError;
import com.money.manager.api.validation.ValidationMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<?> signUp(@Valid @RequestBody User user, BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                List<ValidationError> errors = new ValidationMapper().getErrorList(bindingResult.getAllErrors());
                return new ResponseEntity<>(errors, HttpStatus.OK);
            }
            userService.signUp(user);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/currency")
    public ResponseEntity<String> getUserCurrency() {
        return new ResponseEntity<>(userService.getCurrentUser().getCurrency(), HttpStatus.OK);
    }
}
