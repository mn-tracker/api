package com.money.manager.api.controller;

import com.money.manager.api.dto.EarningDto;
import com.money.manager.api.model.Earning;
import com.money.manager.api.projection.EarningPerMonth;
import com.money.manager.api.service.EarningService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/earnings")
public class EarningController {
    private final EarningService earningService;

    public EarningController(EarningService earningService) {
        this.earningService = earningService;
    }

    @PostMapping()
    public ResponseEntity<?> addEarning(@RequestBody Earning earning) {
        try {
            earningService.addEarning(earning);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @GetMapping()
    public ResponseEntity<Page<EarningDto>> getUserEarnings(@PageableDefault(size = 40, sort = "id", direction = Sort.Direction.DESC) Pageable pageable,
                                                            String dateStart, String dateEnd) {
        return new ResponseEntity<>(earningService.getUserEarnings(pageable, dateStart, dateEnd), HttpStatus.OK);
    }

    @GetMapping("/profit")
    public ResponseEntity<Integer> getUserProfit() {
        return new ResponseEntity<>(earningService.getUserProfit(), HttpStatus.OK);
    }

    @GetMapping("/per_month")
    public ResponseEntity<List<EarningPerMonth>> getEarningPerMonth(int year) {
        return new ResponseEntity<>(earningService.getEarningPerMonth(year), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEarningById(@PathVariable long id) {
        try {
            earningService.deleteEarning(id);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
}
