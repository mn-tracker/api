package com.money.manager.api.controller;

import com.money.manager.api.dto.ExpenseDto;
import com.money.manager.api.model.Expense;
import com.money.manager.api.projection.ExpensePerMonth;
import com.money.manager.api.projection.ExpenseStatistic;
import com.money.manager.api.service.ExpenseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/expenses")
public class ExpenseController {

    private final ExpenseService expenseService;

    public ExpenseController(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }

    @PostMapping()
    public ResponseEntity<?> addExpense(@RequestBody Expense expense) {
        try {
            expenseService.addExpense(expense);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @GetMapping()
    public ResponseEntity<Page<ExpenseDto>> getUserExpenses(@PageableDefault(size = 40, sort = "id", direction = Sort.Direction.DESC) Pageable pageable,
                                                            String dateStart, String dateEnd) {
        return new ResponseEntity<>(expenseService.getUserExpenses(pageable, dateStart, dateEnd), HttpStatus.OK);
    }

    @GetMapping("/statistics")
    public ResponseEntity<List<ExpenseStatistic>> getExpenseStatistics(String dateStart, String dateEnd) {
        return new ResponseEntity<>(expenseService.getExpensesStatistic(dateStart, dateEnd), HttpStatus.OK);
    }

    @GetMapping("/per_month")
    public ResponseEntity<List<ExpensePerMonth>> getExpensePerMonth(int year) {
        return new ResponseEntity<>(expenseService.getExpensePerMonth(year), HttpStatus.OK);
    }

    @GetMapping("/years")
    public ResponseEntity<List<Integer>> getYears() {
        return new ResponseEntity<>(expenseService.getYears(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteExpenseById(@PathVariable long id) {
        try {
            expenseService.deleteExpense(id);
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
}
