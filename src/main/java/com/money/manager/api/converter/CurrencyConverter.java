package com.money.manager.api.converter;


import com.money.manager.api.model.Currency;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static com.money.manager.api.model.Currency.*;


@Converter
public class CurrencyConverter implements AttributeConverter<Currency, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Currency attribute) {
        switch (attribute) {
            case USD:
                return 1;
            case EUR:
                return 2;
            case MDL:
                return 3;
            case RON:
                return 4;
            case RUB:
                return 5;
            default:
                throw new IllegalArgumentException("Unknown name :" + attribute);

        }
    }

    @Override
    public Currency convertToEntityAttribute(Integer dbData) {

        if (dbData == null) return null;

        switch (dbData) {

            case 1:
                return USD;
            case 2:
                return EUR;
            case 3:
                return MDL;
            case 4:
                return RON;
            case 5:
                return RUB;

            default:
                throw new IllegalArgumentException("Unknown value :" + dbData);
        }
    }
}
