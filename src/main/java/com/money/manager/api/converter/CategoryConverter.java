package com.money.manager.api.converter;


import com.money.manager.api.model.Category;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static com.money.manager.api.model.Category.*;


@Converter
public class CategoryConverter implements AttributeConverter<Category, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Category attribute) {
        switch (attribute) {
            case TRANSPORT:
                return 1;
            case CLOTHING:
                return 2;
            case EDUCATION:
                return 3;
            case HEALTH:
                return 4;
            case HOUSE:
                return 5;
            case GIFT:
                return 6;
            case FOOD:
                return 7;
            case SPORT:
                return 8;
            case BEAUTY:
                return 9;
            case UTILITY:
                return 10;
            case INSURANCE:
                return 11;
            default:
                throw new IllegalArgumentException("Unknown name :" + attribute);

        }
    }

    @Override
    public Category convertToEntityAttribute(Integer dbData) {

        if (dbData == null) return null;

        switch (dbData) {

            case 1:
                return TRANSPORT;
            case 2:
                return CLOTHING;
            case 3:
                return EDUCATION;
            case 4:
                return HEALTH;
            case 5:
                return HOUSE;
            case 6:
                return GIFT;
            case 7:
                return FOOD;
            case 8:
                return SPORT;
            case 9:
                return BEAUTY;
            case 10:
                return UTILITY;
            case 11:
                return INSURANCE;

            default:
                throw new IllegalArgumentException("Unknown value :" + dbData);
        }
    }
}
