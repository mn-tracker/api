package com.money.manager.api.mapper;

import com.money.manager.api.dto.EarningDto;
import com.money.manager.api.dto.ExpenseDto;
import com.money.manager.api.model.Earning;
import com.money.manager.api.model.Expense;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class Mapper {

    public ExpenseDto toExpenseDto(Expense expense) {
        ExpenseDto expenseDto = new ExpenseDto();
        expenseDto.setId(expense.getId());
        expenseDto.setCategory(expense.getCategory());
        expenseDto.setCost(expense.getCost());
        String date = expense.getDate().toString();

        //remove seconds and milliseconds from date
        date = date.substring(0, date.length() - 5);

        expenseDto.setDate(date);

        return expenseDto;
    }

    public EarningDto toEarningDto(Earning earning) {
        EarningDto earningDto = new EarningDto();
        earningDto.setId(earning.getId());
        earningDto.setIncome(earning.getIncome());
        String date = earning.getDate().toString();

        //remove seconds and milliseconds from date
        date = date.substring(0, date.length() - 11);

        earningDto.setDate(date);

        return earningDto;
    }

    public Page<EarningDto> toEarningDtoPage(Page<Earning> page) {
        return page.map(this::toEarningDto);
    }

    public Page<ExpenseDto> toExpenseDtoPage(Page<Expense> page) {
        return page.map(this::toExpenseDto);
    }

}
