package com.money.manager.api.projection;

public interface EarningPerMonth {

    int getMonth();

    int getYear();

    int getIncome();
}
