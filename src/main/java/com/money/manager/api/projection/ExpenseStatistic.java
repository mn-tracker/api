package com.money.manager.api.projection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.money.manager.api.converter.CategoryConverter;

public interface ExpenseStatistic {

    @JsonIgnore
    int getCategory();

    int getCost();

    default String getCategoryName() {
        return new CategoryConverter().convertToEntityAttribute(getCategory()).getName();
    }
}
