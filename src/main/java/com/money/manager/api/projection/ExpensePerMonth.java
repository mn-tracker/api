package com.money.manager.api.projection;

public interface ExpensePerMonth {

    int getMonth();

    int getYear();

    int getCost();
}
