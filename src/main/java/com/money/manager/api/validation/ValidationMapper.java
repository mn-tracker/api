package com.money.manager.api.validation;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

public class ValidationMapper {

    public List<ValidationError> getErrorList(List<ObjectError> errorList) {

        List<ValidationError> errors = new ArrayList<>();
        for (ObjectError objectError : errorList) {
            ValidationError error = new ValidationError();
            FieldError fieldError = (FieldError) objectError;
            error.setField(fieldError.getField());
            error.setMessage(fieldError.getDefaultMessage());

            errors.add(error);
        }
        return errors;
    }
}
