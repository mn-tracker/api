package com.money.manager.api.validation;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ValidationError {

    private String field;
    private String message;

}
