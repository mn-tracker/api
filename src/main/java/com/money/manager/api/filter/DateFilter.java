package com.money.manager.api.filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateFilter {

    public static Predicate toDatePredicates(Root<?> root, CriteriaBuilder criteriaBuilder, String dateStart, String dateEnd, long userId) {
        List<Predicate> predicates = new ArrayList<>();
        try {
            if (dateStart != null) {
                Date dtStart = new SimpleDateFormat("yyyy-MM-dd").parse(dateStart);
                predicates.add(criteriaBuilder.or(criteriaBuilder.equal(root.get("date"), dtStart), criteriaBuilder.greaterThan(root.get("date"), dtStart)));
            }
            if (dateEnd != null) {
                Date dtEnd = new SimpleDateFormat("yyyy-MM-dd").parse(dateEnd);
                predicates.add(criteriaBuilder.or(criteriaBuilder.lessThan(root.get("date"), dtEnd), criteriaBuilder.equal(root.get("date"), dtEnd)));
            }
            predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("user").get("id"), userId)));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
