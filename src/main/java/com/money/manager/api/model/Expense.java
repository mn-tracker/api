package com.money.manager.api.model;

import com.money.manager.api.converter.CategoryConverter;
import com.money.manager.api.util.DateUtil;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.Date;

@NoArgsConstructor
@Entity
public class Expense {
    @Id
    @GeneratedValue
    private long id;

    @NotNull(message = "The user is compulsory")
    @ManyToOne(cascade = CascadeType.PERSIST)
    private User user;

    @NotNull(message = "The date is compulsory")
    private Date date;

    @NotNull(message = "The category is compulsory")
    @Convert(converter = CategoryConverter.class)
    private Category category;

    @NotNull(message = "The cost is compulsory")
    private double cost;

    public void setDate(String date) {
        try {
            this.date = DateUtil.formatDate(date, "yyyy-MM-dd HH:mm");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Date getDate() {
        return this.date;
    }

    public String getCategory() {
        return this.category.getName();
    }

    public void setCategory(String category) {
        this.category = Category.valueOf(category.toUpperCase());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
