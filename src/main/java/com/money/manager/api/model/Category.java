package com.money.manager.api.model;

public enum Category {
    TRANSPORT("Transport"),
    CLOTHING("Clothing"),
    EDUCATION("Education"),
    HEALTH("Health"),
    HOUSE("House"),
    GIFT("Gift"),
    FOOD("Food"),
    SPORT("Sport"),
    BEAUTY("Beauty"),
    UTILITY("Utility"),
    INSURANCE("Insurance");

    private final String name;

    Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
