package com.money.manager.api.model;

import com.money.manager.api.util.DateUtil;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.ParseException;
import java.util.Date;

@NoArgsConstructor
@Entity
public class Earning {
    @Id
    @GeneratedValue
    private long id;

    @NotNull(message = "The user is compulsory")
    @ManyToOne(cascade = CascadeType.PERSIST)
    private User user;

    @NotNull(message = "The date is compulsory")
    @Column(nullable = false)
    private Date date;

    @NotNull(message = "The income is compulsory")
    private double income;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(String date) {
        try {
            this.date = DateUtil.formatDate(date,"yyyy-MM-dd");
        } catch (ParseException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }
}