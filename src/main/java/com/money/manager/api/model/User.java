package com.money.manager.api.model;

import com.money.manager.api.converter.CurrencyConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue
    private long id;

    @NotNull(message = "The login is compulsory")
    @Length(min = 5, max = 30, message = "The login must have from 5 to 30 characters")
    @Column(unique = true, length = 30)
    private String login;

    @NotNull(message = "The currency is compulsory")
    @Convert(converter = CurrencyConverter.class)
    private Currency currency;

    @NotNull(message = "The password is compulsory")
    @Length(min = 6, max = 70, message = "The password must have at least 6 characters")
    @Column(length = 70)
    private String password;

    public void setCurrency(String currency) {
        switch (currency) {
            case "₽":
                this.currency = Currency.RUB;
                break;
            case "€":
                this.currency = Currency.EUR;
                break;
            case "$":
                this.currency = Currency.USD;
                break;
            default:
                this.currency = Currency.valueOf(currency.toUpperCase());
        }
    }

    public String getCurrency() {
        return currency.getName();
    }
}
