package com.money.manager.api.model;

public enum Currency {

    USD("$"),
    EUR("€"),
    MDL("MDL"),
    RON("RON"),
    RUB("₽");

    private String name;

    Currency(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}