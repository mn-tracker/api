package com.money.manager.api.service;

import com.money.manager.api.dto.ExpenseDto;
import com.money.manager.api.model.Expense;
import com.money.manager.api.projection.ExpensePerMonth;
import com.money.manager.api.projection.ExpenseStatistic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ExpenseService {

    Page<ExpenseDto> getUserExpenses(Pageable pageable, String dateStart, String dateEnd);

    void addExpense(Expense expense);

    List<ExpenseStatistic> getExpensesStatistic(String dateStart, String dateEnd);

    List<ExpensePerMonth> getExpensePerMonth(int year);

    List<Integer> getYears();

    void deleteExpense(long id);
}
