package com.money.manager.api.service;

import com.money.manager.api.model.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    void signUp(User user);

    long getCurrentUserId();

    String getCurrentUserLogin();

    User getCurrentUser();
}
