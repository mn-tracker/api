package com.money.manager.api.service.serviceImpl;

import com.money.manager.api.dto.EarningDto;
import com.money.manager.api.filter.DateFilter;
import com.money.manager.api.mapper.Mapper;
import com.money.manager.api.model.Earning;
import com.money.manager.api.projection.EarningPerMonth;
import com.money.manager.api.repository.EarningRepository;
import com.money.manager.api.service.EarningService;
import com.money.manager.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EarningServiceImpl implements EarningService {

    private final EarningRepository earningRepository;

    private final UserService userService;

    @Autowired
    private Mapper mapper;

    public EarningServiceImpl(EarningRepository earningRepository, UserService userService) {
        this.earningRepository = earningRepository;
        this.userService = userService;
    }

    @Override
    public Page<EarningDto> getUserEarnings(Pageable pageable, String dateStart, String dateEnd) {
        long userId = userService.getCurrentUserId();
        Page<Earning> earningsPage;

        if (dateStart != null || dateEnd != null) {

            earningsPage = earningRepository.findAll((Specification<Earning>) (root, query, criteriaBuilder) ->

                    DateFilter.toDatePredicates(root, criteriaBuilder, dateStart, dateEnd, userId), pageable);

            return mapper.toEarningDtoPage(earningsPage);

        } else earningsPage = earningRepository.findAllByUserId(userId, pageable);

        return mapper.toEarningDtoPage(earningsPage);
    }

    @Override
    public void addEarning(Earning earning) {
        earning.setUser(userService.getCurrentUser());
        earningRepository.save(earning);
    }

    @Override
    public Integer getUserProfit() {
        return earningRepository.findUserProfit(userService.getCurrentUserId());
    }

    @Override
    public List<EarningPerMonth> getEarningPerMonth(int year) {
        return earningRepository.findEarningPerMonth(userService.getCurrentUserId(), year);
    }

    @Override
    public void deleteEarning(long id) {
        earningRepository.deleteById(id);
    }
}
