package com.money.manager.api.service;

import com.money.manager.api.dto.EarningDto;
import com.money.manager.api.model.Earning;
import com.money.manager.api.projection.EarningPerMonth;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EarningService {

    Page<EarningDto> getUserEarnings(Pageable pageable, String dateStart, String dateEnd);

    void addEarning(Earning earning);

    Integer getUserProfit();

    List<EarningPerMonth> getEarningPerMonth(int year);

    void deleteEarning(long id);
}
