package com.money.manager.api.service.serviceImpl;

import com.money.manager.api.dto.ExpenseDto;
import com.money.manager.api.filter.DateFilter;
import com.money.manager.api.mapper.Mapper;
import com.money.manager.api.model.Expense;
import com.money.manager.api.projection.ExpensePerMonth;
import com.money.manager.api.projection.ExpenseStatistic;
import com.money.manager.api.repository.ExpenseRepository;
import com.money.manager.api.service.ExpenseService;
import com.money.manager.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpenseServiceImpl implements ExpenseService {

    private final ExpenseRepository expenseRepository;

    private final UserService userService;

    @Autowired
    private Mapper mapper;

    public ExpenseServiceImpl(ExpenseRepository expenseRepository, UserService userService) {
        this.expenseRepository = expenseRepository;
        this.userService = userService;
    }

    @Override
    public Page<ExpenseDto> getUserExpenses(Pageable pageable, String dateStart, String dateEnd) {
        long userId = userService.getCurrentUserId();
        Page<Expense> expensesPage;

        if (dateStart != null || dateEnd != null) {

            expensesPage = expenseRepository.findAll((Specification<Expense>) (root, query, criteriaBuilder) ->

                    DateFilter.toDatePredicates(root, criteriaBuilder, dateStart, dateEnd, userId), pageable);

            return mapper.toExpenseDtoPage(expensesPage);

        } else expensesPage = expenseRepository.findAllByUserId(userId, pageable);

        return mapper.toExpenseDtoPage(expensesPage);
    }

    @Override
    public void addExpense(Expense expense) {
        expense.setUser(userService.getCurrentUser());
        expenseRepository.save(expense);
    }

    @Override
    public List<ExpenseStatistic> getExpensesStatistic(String dateStart, String dateEnd) {
        return expenseRepository.findCostSumPerCategoryByDate(userService.getCurrentUserId(),
                dateStart != null ? dateStart : "empty",
                dateEnd != null ? dateEnd : "empty");

    }

    @Override
    public List<ExpensePerMonth> getExpensePerMonth(int year) {
        return expenseRepository.findExpensePerMonth(userService.getCurrentUserId(), year);
    }

    @Override
    public List<Integer> getYears() {
        return expenseRepository.findYears(userService.getCurrentUserId());
    }

    @Override
    public void deleteExpense(long id) {
        expenseRepository.deleteById(id);
    }

}
