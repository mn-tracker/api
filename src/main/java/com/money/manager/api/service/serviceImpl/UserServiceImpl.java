package com.money.manager.api.service.serviceImpl;

import com.money.manager.api.model.User;
import com.money.manager.api.repository.UserRepository;
import com.money.manager.api.service.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserRepository userRepository;

    public UserServiceImpl(BCryptPasswordEncoder bCryptPasswordEncoder, UserRepository userRepository) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public void signUp(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    @Override
    public long getCurrentUserId() {
        return userRepository.findByLogin(this.getCurrentUserLogin()).getId();
    }

    @Override
    public String getCurrentUserLogin() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
    }

    @Override
    public User getCurrentUser() {
        return userRepository.findByLogin(this.getCurrentUserLogin());
    }

}
