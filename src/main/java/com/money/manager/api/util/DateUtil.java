package com.money.manager.api.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    public static Date formatDate(String date, String format) throws ParseException, NullPointerException {
        String time = date.split("T")[1];
        date = date.replace("T", " ");

        SimpleDateFormat dt = new SimpleDateFormat(format);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt.parse(date));

        if (time.equals("21:00:00.000Z"))
            calendar.add(Calendar.DAY_OF_MONTH, 1);

        calendar.add(Calendar.HOUR, 3);
        Date dte = calendar.getTime();
        return dt.parse(dt.format(dte));
    }
}
