package com.money.manager.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EarningDto {

    private long id;
    private double income;
    private String date;

}
