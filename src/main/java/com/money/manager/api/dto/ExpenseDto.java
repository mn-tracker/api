package com.money.manager.api.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ExpenseDto {

    private long id;
    private String category;
    private double cost;
    private String date;

}
