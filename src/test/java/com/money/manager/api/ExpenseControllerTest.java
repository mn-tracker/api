package com.money.manager.api;

import com.money.manager.api.model.User;
import com.money.manager.api.repository.ExpenseRepository;
import com.money.manager.api.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExpenseControllerTest extends AbstractTest {

    @PostConstruct
    private void init() {
        this.setUp();
    }

    @MockBean
    private ExpenseRepository expenseRepository;

    @MockBean
    private UserService userService;

    @Test
    public void addExpense_receiveStatusCreated() throws Exception {
        User user = new User();
        user.setLogin("myLogin");
        user.setPassword(("myPassword"));
        user.setCurrency("MDL");
        user.setId(1L);

        Mockito.doReturn(user)
                .when(userService)
                .getCurrentUser();

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/expenses")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"cost\":\"100\",\"category\":\"Transport\",\"date\":\"2020-01-01T00:00\"}")
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);

    }

}
