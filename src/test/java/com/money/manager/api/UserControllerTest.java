package com.money.manager.api;

import com.money.manager.api.repository.UserRepository;
import com.money.manager.api.service.UserService;
import com.money.manager.api.validation.ValidationError;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UserControllerTest extends AbstractTest {

    @PostConstruct
    private void init() {
        this.setUp();
    }

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserService userService;

    @Test
    public void signUp_receiveAllNullFieldsWarningArray() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/users/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}")
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertNotNull(content);

        List<ValidationError> validationErrors = new ArrayList<>(Arrays.asList(mapFromJson(content, ValidationError[].class)));
        assertEquals(3, validationErrors.size());

        assertTrue(fieldIsPresent(validationErrors, "login"));
        assertTrue(fieldIsPresent(validationErrors, "password"));
        assertTrue(fieldIsPresent(validationErrors, "currency"));

        assertTrue(messageIsPresent(validationErrors, "The login is compulsory"));
        assertTrue(messageIsPresent(validationErrors, "The password is compulsory"));
        assertTrue(messageIsPresent(validationErrors, "The currency is compulsory"));

    }

    private boolean fieldIsPresent(List<ValidationError> validationError, String fieldName) {
        return validationError.stream().anyMatch(o -> o.getField().equals(fieldName));
    }

    private boolean messageIsPresent(List<ValidationError> validationError, String message) {
        return validationError.stream().anyMatch(o -> o.getMessage().equals(message));
    }

    @Test
    public void signUp_receiveLoginAndPasswordLengthWarning() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/users/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"login\":\"l\",\"password\":\"p\",\"currency\":\"MDL\"}")
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);

        String content = mvcResult.getResponse().getContentAsString();
        assertNotNull(content);

        List<ValidationError> validationErrors = new ArrayList<>(Arrays.asList(mapFromJson(content, ValidationError[].class)));
        assertEquals(2, validationErrors.size());

        assertTrue(fieldIsPresent(validationErrors, "login"));
        assertTrue(fieldIsPresent(validationErrors, "password"));

        assertTrue(messageIsPresent(validationErrors, "The login must have from 5 to 30 characters"));
        assertTrue(messageIsPresent(validationErrors, "The password must have at least 6 characters"));
    }

    @Test
    public void signUp_receiveStatusCreated() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/users/sign-up")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"login\":\"myLogin\",\"password\":\"myPassword\",\"currency\":\"MDL\"}")
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(201, status);
    }

}

